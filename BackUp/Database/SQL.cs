﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackUp.Database
{
    public class SQL
    {
        /// <summary>
        /// SQL Connection
        /// </summary>
        public SqlConnection Connection
        {
            get;
            private set;
        }

        /// <summary>
        /// SQL Connection String
        /// </summary>
        public string ConnString
        {
            get;
            private set;
        }
        /// <summary>
        /// SQL Server host
        /// </summary>
        public string Host
        {
            get;
            private set;
        }
        /// <summary>
        /// SQL Server Username
        /// </summary>
        public string User
        {
            get;
            private set;
        }
        /// <summary>
        /// SQL Server Password
        /// </summary>
        public string Pass
        {
            get;
            private set;
        }
        /// <summary>
        /// SQL Connection current Database
        /// </summary>
        public string Database
        {
            get;
            private set;
        }
        /// <summary>
        /// Multiple Active result Sets?
        /// </summary>
        public bool MARS
        {
            get;
            private set;
        }

        public int Timeout
        {
            get;
            private set;
        }

        public bool IsOpen
        {
            get
            {
                return this.Connection != null && this.Connection?.State != ConnectionState.Broken && this.Connection?.State != ConnectionState.Closed;
            }

            private set { }
        }

        public async Task<bool> Open(string host, string user, string pass, string db, bool mars, int timeout)
        {
            if (this.IsOpen)
            {
                "Connection is Open!".MsgBox();
                return false;
            }

            this.Host = host;
            this.User = user;
            this.Pass = pass;
            this.Database = db;
            this.MARS = mars;
            try
            {
                this.ConnString = string.Format("Server={0};UID={1};PWD={2};Initial Catalog={3};MultipleActiveResultSets={4};Timeout={5};",
                    host, user, pass, db, mars, timeout);

                this.Connection = new SqlConnection(ConnString);
                await this.Connection.OpenAsync();
            }
            catch { return false; }

            return IsOpen;
        }

        public void Close()
        {
            if (!this.IsOpen)
                return;

            try
            {
                this.Connection.Close();
            }
            catch { }
        }
    }
}
