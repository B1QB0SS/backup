﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackUp.Database
{
    public class Hi
    {
        public static async Task<bool> TestConnection(string h, string u, string p, int t)
        {
            try
            {
                return await new SQL().Open(h, u, p, "master", true, t);
            }
            catch { return false; }
        }
    }
}
