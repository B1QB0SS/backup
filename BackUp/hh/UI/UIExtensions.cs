﻿using BackUp.Forms;
using System;
using System.Windows.Forms;

namespace BackUp
{
    public static class UIExtensions
    {
        /// <summary>
        /// String to MessageBox
        /// </summary>
        /// <param name="str">Text</param>
        public static void MsgBox(this string str)
        {
            Manage._Instance?.BeginInvoke((MethodInvoker)delegate () { MessageBox.Show(Manage._Instance, str); });
        }

        public static DialogResult YuExit(this string str, string type = "Warning")
        {
            return MessageBox.Show(Manage._Instance, str, type, MessageBoxButtons.YesNo, (MessageBoxIcon)Enum.Parse(typeof(MessageBoxIcon), type));
        }
    }
}
