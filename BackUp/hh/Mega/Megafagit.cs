﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CG.Web.MegaApiClient;
using System.IO;
using BackUp.hh;
using System.Threading.Tasks;

namespace BackUp.hh
{
    public class Megafagit
    {
        private static MegaApiClient _client;
        private static bool _loggedin;
        private static List<Download> _downloads;
        private static IProgress<double> _uploadProgress;

        static Megafagit()
        {
            _client = new MegaApiClient(new Options(bufferSize: 65535));
            _downloads = new List<Download>();
            _loggedin = false;
        }

        public static async Task<bool> Login(string username, string password)
        {
            try
            {
                await _client.LoginAsync(username, password);
                _loggedin = !(_client.GetNodes() == null);
                return _loggedin;
            }
            catch { return false; }
        }

        public static void Logout()
        {
            _client = new MegaApiClient();
            _downloads = new List<Download>();
            _loggedin = false;
        }

        private static IEnumerable<INode> Nodes()
        {
            if (!_loggedin)
                throw new Exception("Not Loggedin!");
            return _client.GetNodes();
        }

        public static async Task<BackUpInfo> UploadtoRoot(string filePath)
        {
            try
            {
                if (!_loggedin) throw new Exception("Not Loggedin!");
                if (!File.Exists(filePath))
                    throw new IOException("File Doesn't Exist!");

                var nodes = Nodes();

                INode root = nodes.Single(n => n.Type == NodeType.Root);
                INode backupsFolder = nodes.FirstOrDefault(n => n.Name == "BackUps");
                if (backupsFolder == null)
                    throw new Exception("Backups Folder is null!");
                if (!nodes.Any(i => i.Name == "BackUps"))
                    backupsFolder = _client.CreateFolder("BackUps", root);
                INode backup = await _client.UploadFileAsync(filePath, backupsFolder, _uploadProgress);
                Uri downloadUrl = await _client.GetDownloadLinkAsync(backup);
                return new BackUpInfo()
                {
                    Date = DateTime.Now,
                    Link = downloadUrl.AbsoluteUri,
                    Name = Path.GetFileName(filePath),
                    Path = "BackUps/" + Path.GetFileName(filePath)
                };
            }
            catch (Exception ex) { ex.ToString().MsgBox(); return null; }
        }

        public static void DownloadBackup(BackUpInfo backup)
        {
            if (!_loggedin)
            { "Not Loggedin".MsgBox(); return; }
            Download download = new Download(_client, backup.Name, backup.Link, _downloads.Count() + 1);
            download.Start();
        }
    }
}
