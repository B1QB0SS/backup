﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BackUp.hh
{
    /// <summary>
    /// Will be used later on. (all forms were dynamically generated this was helpful until i came across a bug) 
    /// </summary>
    //[Obsolete("Useless")]
    public static class Ext
    {
        public static T SetPosition<T>(this Control Result, int x, int y) where T : Control
        {
            Result.Location = new System.Drawing.Point(x, y);
            return (T)Result;
        }

        public static T SetSize<T>(this Control Result, int w, int h) where T : Control
        {
            Result.Size = new System.Drawing.Size(w, h);
            return (T)Result;
        }

        public static T SetText<T>(this Control Result, string Text) where T : Control
        {
            Result.Text = Text;
            return (T)Result;
        }

        public static List<Control> Where(this Control.ControlCollection controls, Type hh)
        {
            List<Control> ctrls = new List<Control>();
            foreach (Control ctrl in controls)
                if (ctrl.GetType() == hh)
                    ctrls.Add(ctrl);
            return ctrls;
        }
    }
}
