﻿namespace BackUp.Forms
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSU = new System.Windows.Forms.Label();
            this.txtSP = new System.Windows.Forms.TextBox();
            this.lblSP = new System.Windows.Forms.Label();
            this.txtSU = new System.Windows.Forms.TextBox();
            this.btnTest = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtSH = new System.Windows.Forms.TextBox();
            this.lblSH = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnMSave = new System.Windows.Forms.Button();
            this.btnMTest = new System.Windows.Forms.Button();
            this.lblMP = new System.Windows.Forms.Label();
            this.txtMP = new System.Windows.Forms.TextBox();
            this.lblMUS = new System.Windows.Forms.Label();
            this.txtMSU = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSU
            // 
            this.lblSU.AutoSize = true;
            this.lblSU.Location = new System.Drawing.Point(20, 67);
            this.lblSU.Name = "lblSU";
            this.lblSU.Size = new System.Drawing.Size(55, 13);
            this.lblSU.TabIndex = 0;
            this.lblSU.Text = "Username";
            // 
            // txtSP
            // 
            this.txtSP.Location = new System.Drawing.Point(84, 86);
            this.txtSP.Name = "txtSP";
            this.txtSP.Size = new System.Drawing.Size(129, 20);
            this.txtSP.TabIndex = 2;
            // 
            // lblSP
            // 
            this.lblSP.AutoSize = true;
            this.lblSP.Location = new System.Drawing.Point(20, 93);
            this.lblSP.Name = "lblSP";
            this.lblSP.Size = new System.Drawing.Size(53, 13);
            this.lblSP.TabIndex = 4;
            this.lblSP.Text = "Password";
            // 
            // txtSU
            // 
            this.txtSU.Location = new System.Drawing.Point(84, 60);
            this.txtSU.Name = "txtSU";
            this.txtSU.Size = new System.Drawing.Size(129, 20);
            this.txtSU.TabIndex = 1;
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(18, 127);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(102, 23);
            this.btnTest.TabIndex = 11;
            this.btnTest.Text = "Test Connection";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTestConn_Click);
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(138, 127);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtSH);
            this.groupBox1.Controls.Add(this.lblSH);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.lblSU);
            this.groupBox1.Controls.Add(this.btnTest);
            this.groupBox1.Controls.Add(this.lblSP);
            this.groupBox1.Controls.Add(this.txtSP);
            this.groupBox1.Controls.Add(this.txtSU);
            this.groupBox1.Location = new System.Drawing.Point(3, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(235, 178);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SQL Connection";
            // 
            // txtSH
            // 
            this.txtSH.Location = new System.Drawing.Point(84, 34);
            this.txtSH.Name = "txtSH";
            this.txtSH.Size = new System.Drawing.Size(129, 20);
            this.txtSH.TabIndex = 0;
            // 
            // lblSH
            // 
            this.lblSH.AutoSize = true;
            this.lblSH.Location = new System.Drawing.Point(20, 41);
            this.lblSH.Name = "lblSH";
            this.lblSH.Size = new System.Drawing.Size(29, 13);
            this.lblSH.TabIndex = 14;
            this.lblSH.Text = "Host";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnMSave);
            this.groupBox2.Controls.Add(this.btnMTest);
            this.groupBox2.Controls.Add(this.lblMP);
            this.groupBox2.Controls.Add(this.txtMP);
            this.groupBox2.Controls.Add(this.lblMUS);
            this.groupBox2.Controls.Add(this.txtMSU);
            this.groupBox2.Location = new System.Drawing.Point(3, 197);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(235, 176);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Mega";
            // 
            // btnMSave
            // 
            this.btnMSave.Enabled = false;
            this.btnMSave.Location = new System.Drawing.Point(138, 132);
            this.btnMSave.Name = "btnMSave";
            this.btnMSave.Size = new System.Drawing.Size(75, 23);
            this.btnMSave.TabIndex = 18;
            this.btnMSave.Text = "Save";
            this.btnMSave.UseVisualStyleBackColor = true;
            // 
            // btnMTest
            // 
            this.btnMTest.Location = new System.Drawing.Point(18, 132);
            this.btnMTest.Name = "btnMTest";
            this.btnMTest.Size = new System.Drawing.Size(102, 23);
            this.btnMTest.TabIndex = 17;
            this.btnMTest.Text = "Test";
            this.btnMTest.UseVisualStyleBackColor = true;
            this.btnMTest.Click += new System.EventHandler(this.btnMTest_Click);
            // 
            // lblMP
            // 
            this.lblMP.AutoSize = true;
            this.lblMP.Location = new System.Drawing.Point(20, 52);
            this.lblMP.Name = "lblMP";
            this.lblMP.Size = new System.Drawing.Size(53, 13);
            this.lblMP.TabIndex = 15;
            this.lblMP.Text = "Password";
            // 
            // txtMP
            // 
            this.txtMP.Location = new System.Drawing.Point(84, 45);
            this.txtMP.Name = "txtMP";
            this.txtMP.Size = new System.Drawing.Size(129, 20);
            this.txtMP.TabIndex = 16;
            // 
            // lblMUS
            // 
            this.lblMUS.AutoSize = true;
            this.lblMUS.Location = new System.Drawing.Point(20, 26);
            this.lblMUS.Name = "lblMUS";
            this.lblMUS.Size = new System.Drawing.Size(55, 13);
            this.lblMUS.TabIndex = 13;
            this.lblMUS.Text = "Username";
            // 
            // txtMSU
            // 
            this.txtMSU.Location = new System.Drawing.Point(84, 19);
            this.txtMSU.Name = "txtMSU";
            this.txtMSU.Size = new System.Drawing.Size(129, 20);
            this.txtMSU.TabIndex = 14;
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(242, 376);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Settings";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox txtSP;
        private System.Windows.Forms.Label lblSP;
        private System.Windows.Forms.TextBox txtSU;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.Label lblSU;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.Label lblMP;
        private System.Windows.Forms.TextBox txtMP;
        public System.Windows.Forms.Label lblMUS;
        private System.Windows.Forms.TextBox txtMSU;
        private System.Windows.Forms.Button btnMSave;
        private System.Windows.Forms.Button btnMTest;
        private System.Windows.Forms.TextBox txtSH;
        private System.Windows.Forms.Label lblSH;
    }
}