﻿using System;
using System.Windows.Forms;

namespace BackUp.Forms
{
    public partial class Manage : Form
    {
        public static Manage _Instance { get; private set; }
        public Manage()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
            this.FormClosing += (s, e) =>
            {
                this.Hide();
            };

            this.Shown += (s, e) =>
            {
                if (_Instance == null)
                    _Instance = this;
            };
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Settings().ShowDialog(this);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult gm = "Y u want to Exit? you might interrupt an upload/download in progress...".YuExit("Warning");
            if (gm == DialogResult.Yes)
                Environment.Exit(0);
            else
                "That was Close.".MsgBox();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
