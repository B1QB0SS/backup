﻿using BackUp.Database;
using BackUp.hh;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackUp.Forms
{
    public partial class Settings : Form
    {
        public static Settings _Instance { get; private set; }
        public Settings()
        {
            InitializeComponent();
            if (_Instance == null)
                _Instance = this;

        }

        private async void btnTestConn_Click(object sender, EventArgs e)
        {
            try
            {
                btnTest.Enabled = false;
                if (groupBox1.Controls.Where(typeof(TextBox)).Any(i => i.Text == string.Empty))
                { "All Fields are Required!".MsgBox(); return; }
                if (await Hi.TestConnection(txtSH.Text, txtSU.Text, txtSP.Text, 3))
                    "Connected!".MsgBox();
                else
                    "Failed to Connect.".MsgBox();
            }
            catch { $"Connection Failed. Error Occured!".MsgBox(); } // bb
            finally { btnTest.Enabled = true; } // Re-Enable the Test Button
        }

        private async void btnMTest_Click(object sender, EventArgs e)
        {
            try
            {
                btnMTest.Enabled = false; // Disabled the Test Button
                if (groupBox2.Controls.Where(typeof(TextBox)).Any(i => i.Text == string.Empty))
                { "All Fields are Required!".MsgBox(); return; }
                if (!await Megafagit.Login(txtMSU.Text, txtMP.Text))
                    "Wrong Username / Password!".MsgBox();
                else
                {
                    "Loggedin Successfully!".MsgBox();
                    for (int i = 0; i < 3; i++)
                    {
                        BackUpInfo uploaded = await Megafagit.UploadtoRoot(@"config\hh.php");
                        File.WriteAllText(@"config\hh.php", uploaded.Link + Environment.NewLine);
                        $"Name: {uploaded.Name}, Link: {uploaded.Link}".MsgBox();
                    }
                    Megafagit.Logout();
                }
            }
            catch (Exception ex){ ex.ToString().MsgBox(); $"Login Failed. Error Occured!".MsgBox(); Megafagit.Logout(); } // Extra check hehexd
            finally { btnMTest.Enabled = true; } // Re-Enable the Test Button
        }
    }
}
