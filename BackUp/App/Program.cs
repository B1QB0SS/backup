﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using BackUp.Forms;
using System.Windows.Forms;

namespace BackUp
{
    class Program
    {
        [MTAThread]
        static void Main(string[] args)
        {
            Application.Run(new Manage());
        }
    }
}
